import React, { Component } from "react"

import {
    Alert,
    Button,
    Row,
    Col,
    Card,
    CardHeader,
    CardFooter,
    CardTitle,
} from 'reactstrap'
import {Authentication} from "../authentication";

export default class DeleteAccount extends Component {
    render(){
        return <Authentication>
            {
                auth => <DeleteAccountDialog token = {auth.token} id = {auth.user.username} logout={auth.logout}/>
            }
        </Authentication>
    }
}


export class DeleteAccountDialog extends Component {

    constructor(props){
        super(props)

        console.log(props)

        this.state = {
            alert: {
                status: ""
            }
        }
    }

    deleteAccount = async () => {

        const response = await fetch(`http://localhost:8081/users/${this.props.id}`, {
            method:'DELETE',
            headers: {
                'Authorization' : this.props.token,
                'Accept': 'application/json;charset=UTF-8',
                'Content-Type': 'application/json;charset=UTF-8'
            }})

        const codigo = response.status;

        if(codigo === 204){
            this.setState(prev => ({...prev, alert: {status: "OK", message: "Cuenta eliminada correctamente"}}))
        }

        else{
            this.setState(prev => ({...prev, alert: {status: "Error", message: "Error al borrar la cuenta"}}))
        }

        this.props.logout()


    }

    render(){
        return <>
            <Card color="primary">
                <CardHeader>
                    <CardTitle className={"login"}>¿Seguro que desea borrar su cuenta?</CardTitle>
                </CardHeader>
                <CardFooter>
                    <Row>
                        <Col>
                            <Button block color={"success"} onClick={this.deleteAccount}>Aceptar</Button>
                        </Col>
                        <Col>
                            <Button block color={"danger"} onClick={""}>Cancelar</Button>
                        </Col>
                    </Row>
                </CardFooter>
            </Card>
            <Alert
                color={this.state.alert.status === "OK" ? "success" : "danger"}
                isOpen = {this.state.alert.status !== ""}
                toggle = { () => this.setState(prev => ({...prev, alert: {status: ""}})) }
            >
                {this.state.alert.message}
            </Alert>
        </>
    }
}
