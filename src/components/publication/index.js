import React, {PureComponent as Component} from 'react'
import {Link} from 'react-router-dom'
import {
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button
} from 'reactstrap';
import {Container, Row, Col, Pagination, PaginationItem, PaginationLink} from 'reactstrap';
import {AuthenticatedOnly, Authentication} from "../authentication";
import Moment from 'react-moment'
import {CommentList} from "../comment";
import PublishComment from "../comment";


export class PublicationList extends Component {
    constructor(props, context) {
        super(props, context)
        this.state = {
            publications: [],
            totalPages: 0
        }
    }

    async componentDidMount() {
        const params = new URLSearchParams(this.props.location.search)
        const page = params.get("page") || 0

        const publicationsRequest = await fetch(`http://localhost:8081/publications?page=${page}`)
        const publicationsResponse = await publicationsRequest.json()

        this.setState(prev => ({...prev, publications: publicationsResponse.content, totalPages: publicationsResponse.totalPages}))
    }

    async componentDidUpdate(prevProps) {
        const prevParams = new URLSearchParams(prevProps.location.search)
        const prevPage = parseInt(prevParams.get("page")) || 0

        const currParams = new URLSearchParams(this.props.location.search)
        const currPage = parseInt(currParams.get("page")) || 0

        if (currPage !== prevPage) {
            const publicationsRequest = await fetch(`http://localhost:8081/publications?page=${currPage}`)
            const publicationsResponse = await publicationsRequest.json()

            this.setState(prev => ({...prev, publications: publicationsResponse.content}))
        }

    }

    render() {

        const page = new URLSearchParams(this.props.location.search).get("page") || 0

        return <>
            {
                this.state.publications.map(publication => <PublicationPreview
                    id={publication.id}
                    author={publication.author}
                    title={publication.title}
                    date={publication.date}
                    summary={publication.summary}
                    keywords={publication.keywords}
                    card1={publication.card1}
                    card2={publication.card2}
                    card3={publication.card3}
                    card4={publication.card4}
                    card5={publication.card5}
                    card6={publication.card6}
                    card7={publication.card7}
                    card8={publication.card8}/>
                )
            }
            <Pagination>
                {
                    Array.from(Array(this.state.totalPages).keys()).map(value =>
                        <PaginationItem key={value} active={parseInt(page) === value}>
                            <PaginationLink tag={Link} to={`/publications?page=${value}`}>
                                {1 + value}
                            </PaginationLink>
                        </PaginationItem>
                    )
                }
            </Pagination>
        </>
    }
}

export class PublicationPreview extends Component {
    render(){
        return <Authentication>
            {
                auth => <PubliPreview
                    userLogged = {auth.user.username}
                    role = {auth.user.roles}
                    id={this.props.id}
                    author={this.props.author}
                    title={this.props.title}
                    date={this.props.date}
                    summary={this.props.summary}
                    keywords={this.props.keywords}
                    card1={this.props.card1}
                    card2={this.props.card2}
                    card3={this.props.card3}
                    card4={this.props.card4}
                    card5={this.props.card5}
                    card6={this.props.card6}
                    card7={this.props.card7}
                    card8={this.props.card8}/>
            }
        </Authentication>
    }
}



export class PubliPreview extends Component {

    render() {
        return <>
                <div className="post">
                <Card>
                    <Container tag={Link} to={`/publications/${this.props.id}`}>
                        <Row>
                            <Col>
                                <CardImg top src={`${this.props.card1}`}/>
                            </Col>
                            <Col>
                                <CardImg top src={`${this.props.card2}`}/>
                            </Col>
                            <Col>
                                <CardImg top src={`${this.props.card3}`}/>
                            </Col>
                            <Col>
                                <CardImg top src={`${this.props.card4}`}/>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <CardImg top src={`${this.props.card5}`}/>
                            </Col>
                            <Col>
                                <CardImg top src={`${this.props.card6}`}/>
                            </Col>
                            <Col>
                                <CardImg top src={`${this.props.card7}`}/>
                            </Col>
                            <Col>
                                <CardImg top src={`${this.props.card8}`}/>
                            </Col>
                        </Row>
                    </Container>
                    <CardBody>
                        <CardTitle tag={Link} to={`/publications/${this.props.id}`}>{this.props.title}</CardTitle>
                        <CardSubtitle> Autor: <a class="author">{this.props.author}</a></CardSubtitle>
                        <CardText>{this.props.summary}</CardText>
                        <CardText id="keywords"> {<span>
                                                        {
                                                            this.props.keywords.length ? this.props.keywords.map((keyword) =>
                                                                (<span> #{keyword} </span>)) : '-'
                                                        }
                                                    </span>}
                        </CardText>
                            <Row>
                                <Col>
                                    <CardText> {<span>
                                                                    {
                                                                        this.props.userLogged === this.props.author ? <Button className={"editButton"} outline color="warning" tag={Link} to={`/modifyPublish/${this.props.id}`}>Modificar</Button> : ""
                                                                    }
                                                </span>}
                                    </CardText>
                                </Col>
                                <Col>
                                    <CardText> {<span>
                                                                    {
                                                                        this.props.userLogged === this.props.author || this.props.role === "ROLE_ADMIN" ? <Button className={"editButton"} outline color="danger" tag={Link} to={`/deletePublish/${this.props.id}`}>Eliminar</Button> : ""
                                                                    }
                                                </span>}
                                    </CardText>
                                </Col>

                            </Row>
                    </CardBody>
                </Card>
            </div>
        </>
    }
}

export class FullPublication extends Component {
    constructor() {
        super()

        this.state = {
            id: "",
            title: "",
            body: "",
            author: "",
            date: "",
            summary: "",
            keywords: [],
            card1: "",
            card2: "",
            card3: "",
            card4: "",
            card5: "",
            card6: "",
            card7: "",
            card8: ""
        }
    }

    async componentDidMount() {
        const publicationRequest = await fetch(`http://localhost:8081/publications/${this.props.match.params.id}`)
        const publicationResponse = await publicationRequest.json()

        this.setState(prev => ({...prev, id: publicationResponse.id, title: publicationResponse.title, body: publicationResponse.body, author: publicationResponse.author,
            date: publicationResponse.date, summary: publicationResponse.summary, keywords: publicationResponse.keywords, card1: publicationResponse.card1, card2: publicationResponse.card2,
            card3: publicationResponse.card3, card4: publicationResponse.card4, card5: publicationResponse.card5, card6: publicationResponse.card6, card7: publicationResponse.card7, card8: publicationResponse.card8}))
    }

    render() {
        return <div class="post">
            <Card>
                <Container>
                    <Row>
                        <Col>
                            <CardImg top src={`${this.state.card1}`}/>
                        </Col>
                        <Col>
                            <CardImg top src={`${this.state.card2}`}/>
                        </Col>
                        <Col>
                            <CardImg top src={`${this.state.card3}`}/>
                        </Col>
                        <Col>
                            <CardImg top src={`${this.state.card4}`}/>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <CardImg top src={`${this.state.card5}`}/>
                        </Col>
                        <Col>
                            <CardImg top src={`${this.state.card6}`}/>
                        </Col>
                        <Col>
                            <CardImg top src={`${this.state.card7}`}/>
                        </Col>
                        <Col>
                            <CardImg top src={`${this.state.card8}`}/>
                        </Col>
                    </Row>
                </Container>
                <CardBody>
                    <CardTitle>{this.state.title}</CardTitle>
                    <CardSubtitle> Autor: <a className="author">{this.state.author}</a></CardSubtitle>
                    <CardText>{this.state.body}</CardText>
                    <CardText align="right"><Moment format="DD/MM/YYYY">{new Date(this.state.date)}</Moment></CardText>
                </CardBody>
            </Card>
            <AuthenticatedOnly requiredRole = {["ROLE_ADMIN", "ROLE_EDITOR", "ROLE_MODERATOR", "ROLE_READER"]}>
                <CommentList example={this.props.match.params.id}></CommentList>
                <PublishComment example={this.props.match.params.id}></PublishComment>
            </AuthenticatedOnly>
        </div>
    }
}


