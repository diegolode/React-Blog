import React, { Component } from "react"
import {
    Alert,
    Button,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    CardSubtitle,
    CardText,
    CardTitle,
    Form,
    FormGroup,
    Input,
} from 'reactstrap'

import Moment from 'react-moment'
import {Authentication} from "../authentication";
import {Link} from "react-router-dom";


export class CommentList extends Component {
    constructor() {
        super()
        this.state = {
            comments: []
        }
    }

    async componentDidMount() {
        const commentsRequest = await fetch(`http://localhost:8081/comments?publicationId=${this.props.example}`)
        const commentsResponse = await commentsRequest.json()

        this.setState(prev => ({...prev, comments: commentsResponse.content}))
    }

    render() {
        return this.state.comments.map(comment => <CommentPreview
            id={comment.id}
            body={comment.body}
            author={comment.author}
            date={comment.date}
            publicationId={comment.publicationId}/>
        )
    }
}

export class CommentPreview extends Component {
    render(){
        return <Authentication>
            {
                auth => <ComPreview
                    userLogged = {auth.user.username}
                    role = {auth.user.roles}
                    id={this.props.id}
                    body={this.props.body}
                    author={this.props.author}
                    date={this.props.date}
                    publicationId={this.props.publicationId}/>
            }
        </Authentication>
    }
}
export class ComPreview extends Component {
    render() {
        return <div className="comment">
            <Card>
                <CardBody>
                    <CardTitle className={"commentAuthor"}>{this.props.author}</CardTitle>
                    <CardSubtitle className={"commentText"}>{this.props.body}</CardSubtitle>
                    <CardText align="right"><Moment format="DD/MM/YYYY">{new Date(this.props.date)}</Moment></CardText>
                    <CardText> {<span>
                                                                    {
                                                                        this.props.userLogged === this.props.author || this.props.role === "ROLE_MODERATOR" || this.props.role === "ROLE_EDITOR" || this.props.role === "ROLE_ADMIN" ? <Button outline color="danger" tag={Link} to={`/deleteComment/${this.props.id}`}>Eliminar</Button> : ""
                                                                    }
                                                </span>}
                    </CardText>
                </CardBody>
            </Card>
        </div>
    }
}

export default class PublishComment extends Component {
    render(){
        return <Authentication>
            {
                auth => <PublishCommentDialog token = {auth.token} pubId = {this.props.example}/>
            }
        </Authentication>
    }
}

export class PublishCommentDialog extends Component {
    constructor(props){
        super(props)

        console.log(props)

        this.state = {
            alert: {
                status: ""
            },
            body : ""
        }
    }

    onBodyChange = event => {
        let value = event.target !== null ? event.target.value : ""
        this.setState(prev => ({...prev, body: value}))
    }


    onPublishButtonClick = () => {
        this.doPublish(this.state.body)
    }

    doPublish = async (bod) => {

        const response = await fetch("http://localhost:8081/comments", {
            method:'POST',
            headers: {
                'Authorization' : this.props.token,
                'Accept': 'application/json;charset=UTF-8',
                'Content-Type': 'application/json;charset=UTF-8'
            },

            body:JSON.stringify({body:bod, publicationId:this.props.pubId})})


        const codigo = response.status;

        if(codigo === 201){
            this.setState(prev => ({...prev, alert: {status: "OK", message: "Comentario publicado correctamente"}}))
        }

        else{
            this.setState(prev => ({...prev, alert: {status: "Error", message: "Error publicando el comentario"}}))
        }


    }

    render() {
        return <>
            <Card color="primary">
                <CardHeader>
                    <CardTitle className={"login"}>Añadir comentario</CardTitle>
                </CardHeader>
                <CardBody>
                    <Form>
                        <FormGroup>
                            <Input type="textarea" value={this.state.body} onChange={this.onBodyChange}/>
                        </FormGroup>
                    </Form>
                </CardBody>
                <CardFooter>
                    <Button block onClick={this.onPublishButtonClick}>Publicar</Button>
                </CardFooter>
            </Card>
            <Alert
                color={this.state.alert.status === "OK" ? "success" : "danger"}
                isOpen = {this.state.alert.status !== ""}
                toggle = { () => this.setState(prev => ({...prev, alert: {status: ""}})) }
            >
                {this.state.alert.message}
            </Alert>
        </>

    }
}