import React, { Component } from "react"

import {
    Alert,
    Button,
    Row,
    Col,
    Card,
    CardHeader,
    CardFooter,
    CardTitle,
} from 'reactstrap'
import {Authentication} from "../authentication";
import {Route} from "react-router-dom";

export default class DeletePublish extends Component {
    render(){
        return <Authentication>
            {
                auth => <DeletePublishDialog token = {auth.token} id = {this.props.match.params.id}/>
            }
        </Authentication>
    }
}


export class DeletePublishDialog extends Component {

    constructor(props){
        super(props)

        console.log(props)

        this.state = {
            alert: {
                status: ""
            }
        }
    }

    deletePublish = async () => {

        const response = await fetch(`http://localhost:8081/publications/${this.props.id}`, {
            method:'DELETE',
            headers: {
                'Authorization' : this.props.token,
                'Accept': 'application/json;charset=UTF-8',
                'Content-Type': 'application/json;charset=UTF-8'
            }})
        const codigo = response.status;

        if(codigo === 204){
            this.setState(prev => ({...prev, alert: {status: "OK", message: "Baraja eliminada correctamente"}}))
        }

        else{
            this.setState(prev => ({...prev, alert: {status: "Error", message: "Error al eliminar la baraja"}}))
        }
    }

    render(){
        return <>
            <Card color="primary">
                <CardHeader>
                    <CardTitle className={"login"}>Confirmar eliminación</CardTitle>
                </CardHeader>
                <CardFooter>
                    <Row>
                        <Col>
                            <Button block color={"success"} onClick={this.deletePublish}>Aceptar</Button>
                        </Col>
                        <Col>
                            <Route>{
                                ({history}) =>
                                <Button block color={"secondary"} onClick={() => history.goBack()}>Volver</Button>
                            }</Route>
                        </Col>
                    </Row>
                </CardFooter>
            </Card>
            <Alert
                color={this.state.alert.status === "OK" ? "success" : "danger"}
                isOpen = {this.state.alert.status !== ""}
                toggle = { () => this.setState(prev => ({...prev, alert: {status: ""}})) }
            >
                {this.state.alert.message}
            </Alert>
        </>
    }
}
