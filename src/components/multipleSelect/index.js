import React from "react";
import PropTypes from "prop-types";
import { Input,Badge, Button } from "reactstrap";

export class MultipleInput extends React.PureComponent {
    static propTypes = {
        color: PropTypes.string,
        onChange: PropTypes.func,
        inline: PropTypes.bool,
        defVal: PropTypes.arrayOf(PropTypes.string)
    };

    static defaultProps = {
        color: "dark",
        onChange: () => {},
        inline: false,
        defVal: []
    };

    constructor(props, context) {
        super(props, context);

        console.log(props)

        this.state = {
            values: props.defVal,
            currentValue: "",
            validInput: true
        };
    }

    componentDidUpdate(prevProps, prevState){
        if(this.props.defVal.length !== prevProps.defVal.length)
            this.setState(prev => ({...prev, values: this.props.defVal}))
    }

    handleRemove = value => {
        this.setState(prev => ({
            ...prev,
            values: prev.values.filter(x => x !== value)
        }));
    };

    handleKeyPress = evt => {
        if (evt.key === "Enter" && this.state.currentValue.length > 0) {
            if (this.state.values.includes(this.state.currentValue)) {
                this.setState(prev => ({
                    ...prev,
                    validInput: false
                }));
            } else {
                this.setState(prev => ({
                    currentValue: "",
                    validInput: true,
                    values: [...prev.values, this.state.currentValue]
                }));
            }
        }
    };

    handleNewValueChange = evt => {
        const value = evt.target.value;

        this.setState(prev => ({
            ...prev,
            currentValue: value
        }));
    };

    render() {

        console.log(this.state)
        return (
            <div
                style={{
                    margin: "1rem",
                    display: "flex",
                    flexDirection: this.props.inline ? "row" : "column"
                }}
            >

                {this.state.values.map(x => (
                    <DeletableBadge
                        key={x}
                        color={this.props.color}
                        value={x}
                        onDelete={this.handleRemove}
                    />
                ))}

                <Input
                    invalid={this.state.validInput ? null : true}
                    value={this.state.currentValue}
                    onChange={this.handleNewValueChange}
                    placeholder="Multiple inputs"
                    onKeyPress={this.handleKeyPress}
                />
            </div>
        );
    }
}

class DeletableBadge extends React.PureComponent {
    handleButtonClick = () => {
        this.props.onDelete(this.props.value);
    };

    render() {
        return (
            <Badge
                color={this.props.color}
                style={{
                    display: "inline-flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                    margin: "0 1px 1px 0"
                }}
            >
                {this.props.value}
                <Button
                    close
                    style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        fontSize: "1.2em",
                        margin: "0 0 0 5px"
                    }}
                    onClick={this.handleButtonClick}
                />
            </Badge>
        );
    }
}