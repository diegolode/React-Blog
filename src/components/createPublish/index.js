import React, { Component } from "react"

import {
    Alert,
    Button,
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    CardTitle,
    Form,
    FormGroup,
    Label,
    Input,
    Modal,
    ModalHeader,
    ModalBody
} from 'reactstrap'

import {MultipleInput} from "../multipleSelect"

import {Authentication} from "../authentication";

export default class Publish extends Component {
    render(){
        return <Authentication>
            {
                auth => <PublishDialog token = {auth.token}/>
            }
        </Authentication>
    }
}

export class PublishDialog extends Component {
    constructor(props){
        super(props)

        console.log(props)

        this.state = {
            alert: {
                status: ""
            },
            id : "",
            title : "",
            body : "",
            author : "",
            date : "",
            summary : "",
            keywords : [],
            card1 : "",
            card2 : "",
            card3 : "",
            card4 : "",
            card5 : "",
            card6 : "",
            card7 : "",
            card8 : ""

        }
    }

    callbackHandlerFunction1 = (newAvatar) => {
        this.setState({
            card1: newAvatar
        });
    }

    callbackHandlerFunction2 = (newAvatar) => {
        this.setState({
            card2: newAvatar
        });
    }

    callbackHandlerFunction3 = (newAvatar) => {
        this.setState({
            card3: newAvatar
        });
    }

    callbackHandlerFunction4 = (newAvatar) => {
        this.setState({
            card4: newAvatar
        });
    }

    callbackHandlerFunction5 = (newAvatar) => {
        this.setState({
            card5: newAvatar
        });
    }

    callbackHandlerFunction6 = (newAvatar) => {
        this.setState({
            card6: newAvatar
        });
    }

    callbackHandlerFunction7 = (newAvatar) => {
        this.setState({
            card7: newAvatar
        });
    }

    callbackHandlerFunction8 = (newAvatar) => {
        this.setState({
            card8: newAvatar
        });
    }

    onTitleChange = event => {
        let value = event.target !== null ? event.target.value : ""
        this.setState(prev => ({...prev, title: value}))
    }

    onBodyChange = event => {
        let value = event.target !== null ? event.target.value : ""
        this.setState(prev => ({...prev, body: value}))
    }


    onSummaryChange = event => {
        let value = event.target !== null ? event.target.value : ""
        this.setState(prev => ({...prev, summary: value}))
    }

    onKeywordsChange = async (values) => {
        this.state.keywords = values
    }


    onPublishButtonClick = () => {
        this.doPublish(this.state.title, this.state.body, this.state.summary,
            this.state.card1, this.state.card2, this.state.card3, this.state.card4, this.state.card5, this.state.card6,
            this.state.card7, this.state.card8, this.state.keywords)
    }

    doPublish = async (tit, bod, sum, c1, c2, c3, c4, c5, c6, c7, c8, kw) => {

        const response = await fetch("http://localhost:8081/publications", {
            method:'POST',
            headers: {
                'Authorization' : this.props.token,
                'Accept': 'application/json;charset=UTF-8',
                'Content-Type': 'application/json;charset=UTF-8'
            },

            body:JSON.stringify({title: tit, body:bod, summary:sum, keywords:kw,
                card1: c1, card2: c2, card3: c3, card4: c4, card5: c5, card6: c6, card7: c7, card8: c8})})


        const codigo = response.status;

        if(codigo === 201){
            this.setState(prev => ({...prev, alert: {status: "OK", message: "Baraja publicada correctamente"}}))
        }

        else{
            this.setState(prev => ({...prev, alert: {status: "Error", message: "Error publicando la baraja"}}))
        }


    }

    render() {
        return <>
            <Card color="primary">
                <CardHeader>
                    <CardTitle className={"login"}>Nueva publicación</CardTitle>
                </CardHeader>
                <CardBody>
                    <Form>
                        <FormGroup>
                            <Label>Título</Label>
                            <Input value={this.state.title} onChange={this.onTitleChange}/>
                        </FormGroup>
                        <FormGroup>
                            <Label>Resumen</Label>
                            <Input value={this.state.summary} onChange={this.onSummaryChange}/>
                        </FormGroup>
                        <FormGroup>
                            <Label>Cuerpo</Label>
                            <Input type="textarea" value={this.state.body} onChange={this.onBodyChange}/>
                        </FormGroup>
                    </Form>
                    <Row>
                        <Col>
                            <ModalExamplePublish handleClickInParent={this.callbackHandlerFunction1} buttonName={"Carta 1"}></ModalExamplePublish>
                        </Col>
                        <Col>
                            <img className={"cardSelected"} src={this.state.card1} alt={""}></img>
                        </Col>
                        <Col>
                            <ModalExamplePublish handleClickInParent={this.callbackHandlerFunction2} buttonName={"Carta 2"}></ModalExamplePublish>
                        </Col>
                        <Col>
                            <img className={"cardSelected"} src={this.state.card2} alt={""}></img>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <ModalExamplePublish handleClickInParent={this.callbackHandlerFunction3} buttonName={"Carta 3"}></ModalExamplePublish>
                        </Col>
                        <Col>
                            <img className={"cardSelected"} src={this.state.card3} alt={""}></img>
                        </Col>
                        <Col>
                            <ModalExamplePublish handleClickInParent={this.callbackHandlerFunction4} buttonName={"Carta 4"}></ModalExamplePublish>
                        </Col>
                        <Col>
                            <img className={"cardSelected"} src={this.state.card4} alt={""}></img>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <ModalExamplePublish handleClickInParent={this.callbackHandlerFunction5} buttonName={"Carta 5"}></ModalExamplePublish>
                        </Col>
                        <Col>
                            <img className={"cardSelected"} src={this.state.card5} alt={""}></img>
                        </Col>
                        <Col>
                            <ModalExamplePublish handleClickInParent={this.callbackHandlerFunction6} buttonName={"Carta 6"}></ModalExamplePublish>
                        </Col>
                        <Col>
                            <img className={"cardSelected"} src={this.state.card6} alt={""}></img>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <ModalExamplePublish handleClickInParent={this.callbackHandlerFunction7} buttonName={"Carta 7"}></ModalExamplePublish>
                        </Col>
                        <Col>
                            <img className={"cardSelected"} src={this.state.card7} alt={""}></img>
                        </Col>
                        <Col>
                            <ModalExamplePublish handleClickInParent={this.callbackHandlerFunction8} buttonName={"Carta 8"}></ModalExamplePublish>
                        </Col>
                        <Col>
                            <img className={"cardSelected"} src={this.state.card8} alt={""}></img>
                        </Col>
                    </Row>
                    <Row><Col><MultipleInput inline defVal={[]} onChange={values => {this.onKeywordsChange(values);}}/></Col></Row>
                </CardBody>
                <CardFooter>
                    <Button block onClick={this.onPublishButtonClick}>Publicar</Button>
                </CardFooter>
            </Card>
            <Alert
                color={this.state.alert.status === "OK" ? "success" : "danger"}
                isOpen = {this.state.alert.status !== ""}
                toggle = { () => this.setState(prev => ({...prev, alert: {status: ""}})) }
            >
                {this.state.alert.message}
            </Alert>
        </>

    }
}

class ModalExamplePublish extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            avatar: "",
            images: []
        };

        this.toggle = this.toggle.bind(this);
    }

    async componentDidMount() {
        const publicationsRequest = await fetch("http://localhost:8081/publications/img")
        const publicationsResponse = await publicationsRequest.json()

        this.setState(prev => ({...prev, images: publicationsResponse}))
    }

    buttonCallbackHandlerFunction = (newAvatar) => {
        this.setState({
            avatar: newAvatar
        });
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
        this.props.handleClickInParent(this.state.avatar)
    }

    toggleExit() {
        this.setState({
            modal: !this.state.modal
        });
    }

    render(){
        return (
            <div>
                <Button className={"cardButton"} color={"info"} onClick={this.toggle}>{this.props.buttonName}</Button>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle} charCode="Aceptar">Añadir carta</ModalHeader>
                    <ModalBody>
                        <div>{
                            this.state.images.map(image => <ButtonComponentPublish handleClickInParent={this.buttonCallbackHandlerFunction} text={"http://localhost:8081/img/" + image}/>
                            )
                        }
                        </div>
                    </ModalBody>
                </Modal>
            </div>
        )
    }
}

class ButtonComponentPublish extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            avatar:"",
            text:""
        };
    }

    handleClick = () => {
        this.setState({
            avatar:this.props.text
        });
        this.props.handleClickInParent(this.props.text);
    }

    render() {
        return (
            <button className={"buttonPicker"} onClick={ this.handleClick }>
                <img className={"imagePicker"} src={this.props.text} alt={""}></img>
            </button>
        );
    }
}
