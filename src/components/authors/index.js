import React, {PureComponent as Component} from 'react'
import {Link} from 'react-router-dom'
import {
    Card, CardImg, CardText, CardBody,
    CardTitle
} from 'reactstrap';
import { PublicationPreview } from "../publication"
import Moment from "react-moment";

export class AuthorList extends Component {
    constructor() {
        super()
        this.state = {
            authors : []
        }
    }

    async componentDidMount() {
        const authorsRequest = await fetch("http://localhost:8081/users?role=ROLE_EDITOR")
        const authorsResponse = await authorsRequest.json()

        const adminsRequest = await fetch("http://localhost:8081/users?role=ROLE_ADMIN")
        const adminsResponse = await adminsRequest.json()

        this.setState(prev => ({...prev, authors: authorsResponse.content.concat(adminsResponse.content)}))

        console.log(this.state.authors)
    }

    render() {
        return this.state.authors.map(author => <AuthorPreview
            username={author.username}
            registerDate={author.registerDate}
            avatar={author.avatar}
        />
        )
    }
}

export class AuthorPreview extends Component {
    render() {
        return <div className="authors">
            <Card>
                <CardBody>
                    <CardImg className={"avatar"} src={`${this.props.avatar}`}/>
                    <CardTitle className={"username"} tag={Link} to={`/authors/${this.props.username}`}>{this.props.username}</CardTitle>
                    <CardText className={"registerDate"}>Fecha de registro: <Moment format="DD/MM/YYYY">{new Date(this.props.registerDate)}</Moment></CardText>
                </CardBody>
            </Card>
        </div>
    }
}

export class AuthorPublications extends Component {
    constructor() {
        super()

        this.state = {
            username: "",
            registerDate: "",
            avatar: "",
            authorPublications: []
        }
    }

    async componentDidMount() {
        const authorRequest = await fetch(`http://localhost:8081/users/${this.props.match.params.id}`)
        const authorResponse = await authorRequest.json()

        const authorPublicationsRequest = await fetch(`http://localhost:8081/publications/?author=${this.props.match.params.id}`)
        const authorPublicationsResponse = await authorPublicationsRequest.json()


        this.setState(prev => ({...prev, username: authorResponse.username, registerDate: authorResponse.registerDate, avatar: authorResponse.avatar, authorPublications: authorPublicationsResponse.content}))
    }

    render() {
        return <>
            {
                <div>
                    <Card>
                        <CardBody>
                            <CardImg className={"avatar"} src={`${this.state.avatar}`}/>
                            <CardTitle className={"username"} tag={Link} to={`/authors/${this.state.username}`}>{this.state.username}</CardTitle>
                            <CardText className={"registerDate"}>Fecha de registro: <Moment format="DD/MM/YYYY">{new Date(this.state.registerDate)}</Moment></CardText>
                        </CardBody>
                    </Card>
                    <h4 id={"publication"}>Publicaciones:</h4>
                </div>
            }
            {
                this.state.authorPublications.map(publication => <PublicationPreview
                    id={publication.id}
                    author={publication.author}
                    title={publication.title}
                    date={publication.date}
                    summary={publication.summary}
                    keywords={publication.keywords}
                    card1={publication.card1}
                    card2={publication.card2}
                    card3={publication.card3}
                    card4={publication.card4}
                    card5={publication.card5}
                    card6={publication.card6}
                    card7={publication.card7}
                    card8={publication.card8}/>
                )
            }

        </>
    }
}
