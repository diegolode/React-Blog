import React, { Component } from "react"

import {
    Alert,
    Button,
    Row,
    Col,
    Card,
    CardHeader,
    CardFooter,
    CardTitle,
} from 'reactstrap'
import {Authentication} from "../authentication";
import {Route} from "react-router-dom";

export default class DeleteComment extends Component {
    render(){
        return <Authentication>
            {
                auth => <DeleteCommentDialog token = {auth.token} id = {this.props.match.params.id}/>
            }
        </Authentication>
    }
}


export class DeleteCommentDialog extends Component {

    constructor(props){
        super(props)

        console.log(props)

        this.state = {
            alert: {
                status: ""
            }
        }
    }

    deleteComment = async () => {

        const response = await fetch(`http://localhost:8081/comments/${this.props.id}`, {
            method:'DELETE',
            headers: {
                'Authorization' : this.props.token,
                'Accept': 'application/json;charset=UTF-8',
                'Content-Type': 'application/json;charset=UTF-8'
            }})

        const codigo = response.status;

        if(codigo === 204){
            this.setState(prev => ({...prev, alert: {status: "OK", message: "Comentario eliminado correctamente"}}))
        }

        else{
            this.setState(prev => ({...prev, alert: {status: "Error", message: "Error al eliminar el comentario"}}))
        }




    }

    render(){
        return <>
            <Card color="primary">
                <CardHeader>
                    <CardTitle className={"login"}>Confirmar eliminación</CardTitle>
                </CardHeader>
                <CardFooter>
                    <Row>
                        <Col>
                            <Route>{
                                ({history}) =>
                                    <Button block color={"success"} onClick={this.deleteComment}>Aceptar</Button>
                            }</Route>
                        </Col>
                        <Col>
                            <Route>{
                                ({history}) =>
                                    <Button block color={"secondary"} onClick={() => history.goBack()}>Volver</Button>
                            }</Route>
                        </Col>
                    </Row>
                </CardFooter>
            </Card>
            <Alert
                color={this.state.alert.status === "OK" ? "success" : "danger"}
                isOpen = {this.state.alert.status !== ""}
                toggle = { () => this.setState(prev => ({...prev, alert: {status: ""}})) }
            >
                {this.state.alert.message}
            </Alert>
        </>
    }
}
