import React, { Component } from 'react'
import { Switch, Route, Redirect } from "react-router-dom"
import NavigationBar from "../navbar"
import { Container } from "reactstrap"
import { PublicationList, FullPublication } from "../publication"
import { AuthorList, AuthorPublications} from "../authors";
import Login from "../login"
import Logout from "../logout"
import Register from "../register"
import Publish from "../createPublish"
import ModifyPublish from "../modifyPublish"
import DeletePublish from "../deletePublish"
import DeleteComment from "../deleteComment"
import DeleteAccount from "../deleteAccount";
import {UserList} from "../adminUsers";
import DeleteUserAccount from "../deleteUserAccount"

export default class Layout extends Component {

    render(){
        return <>
            <NavigationBar/>
            <Container className = "pt-3" tag = "main">
                <Switch>
                    <Route path = "/publications/:id" component = { FullPublication }/>
                    <Route path = "/publications" component = { PublicationList } />
                    <Route path = "/authors/:id" component = { AuthorPublications }/>
                    <Route path = "/authors" component = { AuthorList } />
                    <Route path = "/login" component = { Login } />
                    <Route path = "/logout" component = { Logout } />
                    <Route path = "/register" component = { Register } />
                    <Route path = "/publish" component = { Publish } />
                    <Route path = "/modifyPublish/:id" component = { ModifyPublish } />
                    <Route path = "/deletePublish/:id" component = { DeletePublish } />
                    <Route path = "/deleteComment/:id" component = { DeleteComment } />
                    <Route path = "/deleteAccount" component = { DeleteAccount } />
                    <Route path = "/adminUsers" component = { UserList } />
                    <Route path = "/deleteUserAccount/:id" component = { DeleteUserAccount } />
                    <Redirect from = "/" to = "/publications"/>
                </Switch>
            </Container>
        </>
    }
}