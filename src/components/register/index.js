import React, { Component } from "react"

import {
    Alert,
    Button,
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    CardTitle,
    Form,
    FormGroup,
    Label,
    Input
} from 'reactstrap'

import {Modal, ModalHeader, ModalBody} from 'reactstrap';
import {Authentication} from "../authentication";
import {DeleteUserAccountDialog} from "../deleteUserAccount";

export default class Register extends Component {
    render(){
        return <Authentication>
            {
                auth => <RegisterDialog login={auth.login}/>
            }
        </Authentication>
    }
}

export class RegisterDialog extends Component {
    constructor(props){
        super(props)

        this.state = {
            alert: {
                status: ""
            },
            username : "",
            password : "",
            email : "",
            avatar : ""

        }
    }

    callbackHandlerFunction = (newAvatar) => {
        this.setState({
            avatar: newAvatar
        });
    }

    onUsernameChange = event => {
        let value = event.target !== null ? event.target.value : ""
        this.setState(prev => ({...prev, username: value}))
    }

    onPasswordChange = event => {
        let value = event.target !== null ? event.target.value : ""
        this.setState(prev => ({...prev, password: value}))
    }

    onFullnameChange = event => {
        let value = event.target !== null ? event.target.value : ""
        this.setState(prev => ({...prev, fullname: value}))
    }

    onEmailChange = event => {
        let value = event.target !== null ? event.target.value : ""
        this.setState(prev => ({...prev, email: value}))
    }

    onRegisterButtonClick = () => {
        this.doRegister(this.state.username, this.state.password, this.state.fullname, this.state.email, this.state.avatar)
    }

    doRegister = (user, pass, name, mail, avatar) => {
        fetch("http://localhost:8081/users", {method:'POST',
                                              headers:{'Accept': 'application/json;charset=UTF-8', 'Content-Type': 'application/json;charset=UTF-8'},
                                              body:JSON.stringify({username:user, password:pass, fullname:name, email:mail, avatar:avatar})})
            .then(response => {

                const codigo = response.status;

                let error = 0

                if(codigo === 201){
                    this.setState(prev => ({...prev, alert: {status: "OK", message: "Usuario registrado correctamente"}}))
                    this.props.login(this.state.username, this.state.password)

                }
                else if(codigo === 409) {
                    this.setState(prev => ({...prev, alert: {status: "Error", message: "El usuario ya existe"}}))
                    error = 1
                }

                else if(codigo !== 201 && codigo !== 409 && error === 0){
                    this.setState(prev => ({...prev, alert: {status: "Error", message: "El usuario ya existe"}}))
                }

            })

    }

    render() {
        return <>
            <Card color="primary">
                <CardHeader>
                    <CardTitle className={"login"}>Registro</CardTitle>
                </CardHeader>
                <CardBody>
                    <Form>
                        <FormGroup>
                            <Label>Usuario</Label>
                            <Input value={this.state.username} onChange={this.onUsernameChange} required/>
                        </FormGroup>
                        <FormGroup>
                            <Label>Contraseña</Label>
                            <Input type="password" value={this.state.password} onChange={this.onPasswordChange}/>
                        </FormGroup>
                        <FormGroup>
                            <Label>Nombre</Label>
                            <Input value={this.state.fullname} onChange={this.onFullnameChange}/>
                        </FormGroup>
                        <FormGroup>
                            <Label>Email</Label>
                            <Input value={this.state.email} onChange={this.onEmailChange}/>
                        </FormGroup>
                    </Form>
                    <Row>
                        <Col>
                            <ModalExample handleClickInParent={this.callbackHandlerFunction}></ModalExample>
                        </Col>
                        <Col>
                            <img className={"avatarSelected"} src={this.state.avatar} alt={""}></img>
                        </Col>
                    </Row>
                </CardBody>
                <CardFooter>
                    <Button block onClick={this.onRegisterButtonClick}>Registro</Button>
                </CardFooter>
            </Card>
            <Alert
                color={this.state.alert.status === "OK" ? "success" : "danger"}
                isOpen = {this.state.alert.status !== ""}
                toggle = { () => this.setState(prev => ({...prev, alert: {status: ""}})) }
            >
                {this.state.alert.message}
            </Alert>
        </>
    }
}

class ModalExample extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            avatar: "",
            images: []
        };

        this.toggle = this.toggle.bind(this);
    }

    async componentDidMount() {
        const publicationsRequest = await fetch("http://localhost:8081/publications/img")
        const publicationsResponse = await publicationsRequest.json()

        this.setState(prev => ({...prev, images: publicationsResponse}))
    }

    buttonCallbackHandlerFunction = (newAvatar) => {
        this.setState({
            avatar: newAvatar
        });
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
        this.props.handleClickInParent(this.state.avatar)
    }

    toggleExit() {
        this.setState({
            modal: !this.state.modal
        });
    }

    render(){
        return (
            <div>
                <Button className={"avatarButton"} color={"info"} onClick={this.toggle}>Seleccionar avatar</Button>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle} charCode="Aceptar">Selecciona avatar</ModalHeader>
                    <ModalBody>
                        <div>{
                            this.state.images.map(image => <ButtonComponent handleClickInParent={this.buttonCallbackHandlerFunction} text={"http://localhost:8081/img/" + image}/>
                            )
                        }
                        </div>
                    </ModalBody>

                </Modal>
            </div>
        )
    }
}

class ButtonComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            avatar:"",
            text:""
        };
    }

    handleClick = () => {
        this.setState({
            avatar:this.props.text
        });
        this.props.handleClickInParent(this.props.text);
    }

    render() {
        return (
            <button className={"buttonPicker"} onClick={ this.handleClick }>
                <img className={"imagePicker"} src={this.props.text} alt={""}></img>
            </button>
        );
    }
}
