import React, { PureComponent as Component } from 'react'
import {
    Navbar,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from 'reactstrap'

import { Link } from 'react-router-dom'
import {AuthenticatedOnly, Authentication, UnauthenticatedOnly} from "../authentication";

export default class NavigationBar extends Component {
    render(){
        return <Authentication>
            {
                auth => <NavBar user = {auth.user.username}/>
            }
        </Authentication>
    }
}

export class NavBar extends Component {

    render(){
        return <Navbar sticky = "top" expand dark color = "primary">
            <NavbarBrand tag={Link} to="/">Top Royale</NavbarBrand>
            <Nav className="ml-auto" navbar>
                <NavItem>
                    <NavLink tag={Link} to="/">Barajas</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink tag={Link} to="/authors">Autores</NavLink>
                </NavItem>
                <AuthenticatedOnly requiredRole = {["ROLE_ADMIN", "ROLE_EDITOR", "ROLE_MODERATOR"]}>
                    <NavItem>
                        <NavLink tag={Link} to="/adminUsers">Administrar usuarios</NavLink>
                    </NavItem>
                </AuthenticatedOnly>
                <UnauthenticatedOnly>
                    <NavItem>
                        <NavLink tag={Link} to="/register">Registro</NavLink>
                    </NavItem>
                </UnauthenticatedOnly>
                <UnauthenticatedOnly>
                    <NavItem>
                        <NavLink tag={Link} to="/login">Iniciar sesión</NavLink>
                    </NavItem>
                </UnauthenticatedOnly>
                <AuthenticatedOnly requiredRole = {["ROLE_ADMIN", "ROLE_EDITOR"]}>
                    <UncontrolledDropdown nav inNavbar>
                        <DropdownToggle nav caret>
                            Mis publicaciones
                        </DropdownToggle>
                        <DropdownMenu right>
                            <DropdownItem tag={Link} to="/publish">
                                Añadir publicación
                            </DropdownItem>
                            <DropdownItem tag={Link} to={"authors/" + this.props.user}>
                                Gestionar publicaciones
                            </DropdownItem>
                        </DropdownMenu>
                    </UncontrolledDropdown>
                </AuthenticatedOnly>
                <AuthenticatedOnly>
                    <UncontrolledDropdown nav inNavbar>
                        <DropdownToggle nav caret>
                            {this.props.user}
                        </DropdownToggle>
                        <DropdownMenu right>
                            <DropdownItem tag={Link} to="/deleteAccount">
                                Borrar cuenta
                            </DropdownItem>
                            <DropdownItem tag={Link} to="/logout">
                                Cerrar sesión
                            </DropdownItem>
                        </DropdownMenu>
                    </UncontrolledDropdown>
                </AuthenticatedOnly>
            </Nav>
        </Navbar>
    }
}

NavbarBrand.propTypes = {
    color: "red"
}
