import {Component} from "react";
import {AuthenticatedOnly, Authentication} from "../authentication";
import {
    Alert,
    Button,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    CardImg,
    CardSubtitle,
    CardText,
    CardTitle,
    Col,
    Form,
    FormGroup,
    Input,
    Row
} from 'reactstrap'
import Moment from "react-moment";
import {Link, Route} from "react-router-dom";
import React from "react";

export class UserList extends Component {
    constructor() {
        super()
        this.state = {
            users: []
        }
    }

    async componentDidMount() {
        const usersRequest = await fetch(`http://localhost:8081/users`)
        const usersResponse = await usersRequest.json()

        this.setState(prev => ({...prev, users: usersResponse.content}))
    }

    render() {
        return this.state.users.map(user => <UserPreview
            id={user.username}
            roleUser={user.role}
            avatar={user.avatar}
            registerDate={user.registerDate}
            enabled={user.enabled}
            />
        )
    }

}

export class UserPreview extends Component {
    render(){
        return <Authentication>
            {
                auth => <UserView
                    token = {auth.token}
                    role = {auth.user.roles}
                    id={this.props.id}
                    avatar={this.props.avatar}
                    roleUser={this.props.roleUser}
                    registerDate={this.props.registerDate}
                    enabled={this.props.enabled}
                />
            }
        </Authentication>
    }
}
export class UserView extends Component {


    onRoleChange = async evt => {
        const value = evt.target.value

        const req = await fetch(`http://localhost:8081/users/${this.props.id}`, {
            method: "PATCH",
            headers: {
                "Authorization": this.props.token,
                "Content-Type": "application/json"
            },
            body: JSON.stringify([
                { op: "replace", path: "/role", value: value}
            ])
        })

    }

    onEnabledChange = async evt => {
        const value = evt.target.value

        const req = await fetch(`http://localhost:8081/users/${this.props.id}`, {
            method: "PATCH",
            headers: {
                "Authorization": this.props.token,
                "Content-Type": "application/json"
            },
            body: JSON.stringify([
                { op: "replace", path: "/enabled", value: value}
            ])
        })

    }

    render() {
        return <div className="user">
            <Card>
                <CardBody>
                    <Row>
                        <Col className={"userAvatar"}>
                            <CardImg className={"userImg"} top src={`${this.props.avatar}`}/>
                        </Col>
                        <Col>
                            <CardTitle className={"userId"}>{this.props.id}</CardTitle>
                            <CardText className={"userDate"}>Reg: <Moment format="DD/MM/YYYY">{new Date(this.props.registerDate)}</Moment></CardText>
                        </Col>
                        <Col>
                            <CardText className={"expelUser"} > {<span>
                                                                    {
                                                                        this.props.role === "ROLE_ADMIN" ? <Button size={"lg"} className={"expelButton"} color="danger" tag={Link} to={`/deleteUserAccount/${this.props.id}`}>Expulsar</Button> : ""
                                                                    }
                                                </span>}
                            </CardText>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Route>{
                                ({history}) =>
                                    <Input type="select" className={"selectRole"} value={this.props.roleUser} onChange={this.onRoleChange}>
                                        <option value="ROLE_READER">Lector</option>
                                        <option value="ROLE_MODERATOR">Moderador</option>
                                        <option value="ROLE_EDITOR">Editor</option>
                                        <option value="ROLE_ADMIN">Administrador</option>
                                    </Input>
                            }</Route>
                        </Col>
                        <AuthenticatedOnly requiredRole = {["ROLE_ADMIN"]}>
                        <Col>
                            <Route>{
                                ({history}) =>
                                    <Button block color={"info"} onClick={() => history.goBack()}>Cambiar rol</Button>
                            }</Route>
                        </Col>
                        </AuthenticatedOnly>
                    </Row>
                    <Row className={"selectBan"}>
                        <Col>
                            <Route>{
                                ({history}) =>
                                    <Input type="select" className={"selectRole"} value={this.props.enabled} onChange={this.onEnabledChange}>
                                        <option value="true">No</option>
                                        <option value="false">Sí</option>
                                    </Input>
                            }</Route>
                        </Col>
                        <Col>
                            <Route>{
                                ({history}) =>
                                    <Button block color={"primary"} onClick={() => history.goBack()}>Suspender</Button>
                            }</Route>
                        </Col>
                    </Row>
                </CardBody>
            </Card>
        </div>
    }
}